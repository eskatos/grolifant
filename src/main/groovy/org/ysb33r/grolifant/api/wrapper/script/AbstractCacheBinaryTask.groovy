/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.wrapper.script

import groovy.transform.CompileStatic

/** Base class for tasks that can cache arbitrary binaries.
 *
 * @author Schalk W. Cronjé
 *
 * @deprecated Use{@link org.ysb33r.grolifant.api.v4.wrapper.script.AbstractCacheBinaryTask} instead.
 *
 * @since 0.14
 */
@CompileStatic
@Deprecated
@SuppressWarnings(['LineLength', 'ClassNameSameAsSuperclass', 'AbstractClassWithoutAbstractMethod'])
abstract class AbstractCacheBinaryTask extends org.ysb33r.grolifant.api.v4.wrapper.script.AbstractCacheBinaryTask {
    protected AbstractCacheBinaryTask(String locationPropertiesDefaultName) {
        super(locationPropertiesDefaultName)
    }
}
