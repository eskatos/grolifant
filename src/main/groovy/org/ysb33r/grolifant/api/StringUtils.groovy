/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.provider.Provider

/** A collection of utilities for converting to strings.
 *
 * @deprecated Use methods from {@link org.ysb33r.grolifant.api.v4.StringUtils} instead.
 */
@CompileStatic
@Deprecated
class StringUtils {

    /** Converts most things to a string. Closures are evaluated as well.
     *
     * @param stringy An object that can be converted to a string or a closure that
     *   can be evaluated to something that can be converted to a string.
     * @return A string object
     *
     * @deprecated
     */
    @SuppressWarnings('CouldBeSwitchStatement')
    static String stringize(final Object stringy) {
        org.ysb33r.grolifant.api.v4.StringUtils.stringize(stringy)
    }

    /** Converts a collection of most things to a list of strings. Closures are evaluated as well.
     *
     * @param Iterable list of objects that can be converted to strings, including closure that can be evaluated
     *   into objects that can be converted to strings.
     * @return A list of strings
     * @deprecated
     */
    static List<String> stringize(final Iterable<?> stringyThings) {
        org.ysb33r.grolifant.api.v4.StringUtils.stringize(stringyThings)
    }

    /** Updates a Provider.
     *
     * If the Provider is a {@link org.gradle.api.provider.Property} it will be updated in place,
     * otherwise the provider will be assigned a new Provider instance.
     *
     * This method requires Gradle 4.3 at minimum
     *
     * @param project Project context for creating providers
     * @param provider Current provider
     * @param stringy Value that should be lazy-resolved.
     *
     * @deprecated
     *
     * @since 0.16
     */
    static void updateStringProperty(Project project, Provider<String> provider, Object stringy) {
        org.ysb33r.grolifant.api.v4.StringUtils.updateStringProperty(project, provider, stringy)
    }
}
