/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api

import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors

/** Resolves properties in a certain i.e. like SprintBoot, but
 * less functionality to suite Gradle context.
 *
 * @deprecated Use{@link org.ysb33r.grolifant.api.v4.PropertyResolver} instead.
 *
 * @author Schalk W. Cronjé
 *
 * @since 0.15.0
 */
@CompileStatic
@Deprecated
@SuppressWarnings(['LineLength', 'ClassNameSameAsSuperclass'])
@InheritConstructors
class PropertyResolver extends org.ysb33r.grolifant.api.v4.PropertyResolver {
    public static final PropertyResolveOrder PROJECT_SYSTEM_ENV = org.ysb33r.grolifant.api.v4.PropertyResolver.PROJECT_SYSTEM_ENV
    public static final PropertyResolveOrder SYSTEM_ENV_PROPERTY = org.ysb33r.grolifant.api.v4.PropertyResolver.SYSTEM_ENV_PROPERTY
}
