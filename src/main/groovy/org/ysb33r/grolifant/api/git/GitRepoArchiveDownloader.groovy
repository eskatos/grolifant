/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.git

import groovy.transform.CompileStatic
import org.gradle.api.Project

/** Downloads an archive of a Git repository.
 *
 * @deprecated Use {@link org.ysb33r.grolifant.api.v4.git.GitRepoArchiveDownloader} instead.
 *
 * @since 0.8
 */
@CompileStatic
@Deprecated
@SuppressWarnings(['AbstractClassWithoutAbstractMethod', 'ClassNameSameAsSuperclass', 'LineLength'])
class GitRepoArchiveDownloader extends org.ysb33r.grolifant.api.v4.git.GitRepoArchiveDownloader {
    @SuppressWarnings('DuplicateStringLiteral')
    GitRepoArchiveDownloader(final CloudGitDescriptor descriptor, final Project project) {
        super(descriptor, project)
    }
}
