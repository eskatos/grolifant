== Task Utilities

=== Lazy evaluate objects to task instances

The link:{gradle-api}/org/gradle/api/Task.html[Task.dependOn] method allows various ways for objects to be evaluated to tasks at a later point in time.
Unfortunately this kind of functionality is not available via a public API and plugin authors would want to create links between tasks for domain reasons has to find other ways of creating lazy-evaluated task dependencies.
In 0.17.0 we added the link:{groovydocv4}/TaskUtils.html[TaskUtils.taskize] to help relieve this problem.

[source,groovy]
----
Task myTask = TaskUtils.tasksize(project, "${myTask}") // <1>
List<Task> myTasks = TaskUtils.tasksize(project, [myTask1, myTaskProvider2]) // <2>
----
<1> Resolves a single item to a task within a project context. Single items could be recursively evaluated if applicable, for example Closures returning strings.
<2> Resolves an iterable sequence of items to a list of tasks within a project context. Items are recursively evaluated if necessary.

The fully supported list of items are always described in the link:{groovydocv4}/TaskUtils.html[API documentation], but the following list can be used as a general guideline:

.Supported single object types
* Gradle task.
* Gradle link:{gradle-api}/org/gradle/api/tasks/TaskProvider.html[TaskProvider] (Gradle 4.8+).
* Grolifant link:{groovydocv4}/TaskProvider.html[TaskProvider].
* Any link:{jdk-api}/java/lang/CharSequence.html[CharSequence] including String and GString.
* Any Gradle link:{gradle-api}/org/gradle/api/provider/Provider.html[Provider] to the above types
* Any link:{jdk-api}/java/util/concurrent/Callable.html[Callable] implementation including Groovy Closures that return one of the above values.

.Additional types for the iterable version
* Any iterable sequence of the above types
* A link:{jdk-api}/java/util/Map.html[Map] for which the values are evaluated. (The keys are ignored).

=== Lazy create tasks

In Gradle 4.9, functionality was added to allow for lazy creation and configuration of tasks.
Although this provides the ability for a Gradle build to be configured much quicker, it creates a dilemma for plugin authors wanting to be backwards compatible.

With the link:{groovydocv4}++/TaskProvider.html++[TaskProvider] API in Grolifant it is now possible for plugin authors to lazy-create tasks in Gradle 4.9, but automatically fall back to straight creation of tasks on older versions.

[source,groovy]
----
// The following imports are assumed
import org.ysb33r.grolifant.api.v4.TaskProvider
import static org.ysb33r.grolifant.api.v4.TaskProvider.registerTask

include::{compatdirv4}/TaskProviderSpec.groovy[tags=register_task,indent=0]

include::{compatdirv4}/TaskProviderSpec.groovy[tags=configure_task,indent=0]
----
<1> Register a task and configure it. In case of Gradle 4.9+ the configuration will queued until such time that the task is actually needed.
<2> Add a configuration to the existing task. In case of Gradle 4.9+ the configuration is added as an action to be executed later. In earlier versions the task will be configured immediately.

Kotlin and Java implementations can use `Action` instances instead of closures.

