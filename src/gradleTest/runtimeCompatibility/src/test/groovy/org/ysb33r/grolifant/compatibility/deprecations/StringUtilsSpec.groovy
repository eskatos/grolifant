/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.deprecations

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.StringUtils
import org.ysb33r.grolifant.internal.v4.LegacyLevel
import spock.lang.IgnoreIf
import spock.lang.Specification

class StringUtilsSpec extends Specification {

    void 'Convert a single string'() {
        expect:
        // tag::stringize_string[]
        StringUtils.stringize('foo') == 'foo'
        StringUtils.stringize(new File('foo')) == 'foo'
        StringUtils.stringize { 'foo' } == 'foo'
        // end::stringize_string[]
    }

    void 'Convert a list of strings'() {
        expect:
        // tag::stringize_collection[]
        StringUtils.stringize(['foo1', new File('foo2'), { 'foo3' }]) == ['foo1', 'foo2', 'foo3']
        // end::stringize_collection[]
    }

//    void 'Update a provider'() {
//        setup:
//        Project project = ProjectBuilder.builder().build()
//        Provider<String> oldProv = project.provider({ -> 'foo' } as Callable<String>)
//
//        when:
//        StringUtils.updateStringProperty(project, oldProv, 'bar')
//
//        then:
//        oldProv.get() == 'bar'
//    }

    @IgnoreIf({ LegacyLevel.PRE_4_3 })
    void 'Update a property'() {
        setup:
        Project project = ProjectBuilder.builder().build()
        def oldProv = project.objects.property(String)
        oldProv.set('foo')
        def cachedProv = oldProv

        when:
        StringUtils.updateStringProperty(project, oldProv, 'bar')

        then:
        oldProv.get() == 'bar'
        cachedProv.get() == 'bar'
    }
}